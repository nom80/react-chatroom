# README #

React + Firebase ChatRoom exercise.

### Setup ###

1. Clone the repo.
2. In a terminal, run the command: python -m SimpleHTTPServer 8000
3. Browse to: http://localhost:8000/

Users can log in by entering a Nickname and clicking "Log in as a guest"
or by (optionally entering a Nickname) and clicking "Log in with Google".
Nicknames for returning Google-Account users persist.