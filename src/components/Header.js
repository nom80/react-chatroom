import React, { Component } from 'react';
import Login from './Login';

class Header extends Component {
  constructor(props){
    super(props)
    this.handleUserUpdate = this.handleUserUpdate.bind(this);
    this.handleAuthLogout = this.handleAuthLogout.bind(this);
  }

  //update user state in parent component
  handleUserUpdate(user){
    this.props.onUserUpdate(user);
  }
  //update logout state in parent component
  handleAuthLogout(){
    this.props.onAuthLogout();
  }

  render(){
    const user = this.props.user;

    let greet = null;

    if (user){ //logged in
      greet =
        <hgroup>
          <h1>Hello, {user.displayName}!</h1>
        </hgroup>
    } else { //logged out
      greet =
        <hgroup>
          <h1>Hello, newcomer!</h1>
          <h2>Please sign in:</h2>
        </hgroup>
    }
    return(
      <header>
        {greet}
        <Login
          user={this.props.user}
          auth={this.props.auth} provider={this.props.provider}
          onUserUpdate={this.handleUserUpdate}
          onAuthLogout={this.handleAuthLogout}
        />
      </header>
    );
  }
}

export default Header
