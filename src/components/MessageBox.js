import React, { Component } from 'react';

class MessageBox extends Component {
  constructor(props){
    super(props)
    this.onChange = this.onChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      message: ''
    }
  }

  //update state on input change
  onChange(e){
    this.setState({
      message: e.target.value
    })
  }

  //submit message on 'enter'
  onKeyDown(e){
    if (e.which === 13){
      e.preventDefault();
      this.onSubmit(e);
    }
  }

  //handle message submit
  onSubmit(e){
    e.preventDefault();
    const newMessage = this.state.message.trim();

    if (newMessage){//must have a message
      //update db with new message
      let dbCon = this.props.db.database().ref('/messages');
      dbCon.push({
        name: this.props.userName,
        message: newMessage,
        timestamp: + new Date()
      });
    }
    //clear state for next message
    this.setState({
      message: ''
    })
  }

  render(){
    return (
      <div className="message-box">
        <textarea
          placeholder="Message"
          value={this.state.message}
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}></textarea>
      </div>
    )
  }
}

export default MessageBox
