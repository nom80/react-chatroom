import React, { Component } from 'react';
import Message from './Message';

class MessageList extends Component {

  constructor(props){
    super(props)
    this.state = {
      messages: []
    }
  }

  componentDidMount(){
    // query db for messages
    this.props.db.database().ref('messages/').on('value', (snapshot) => {
      const currentMessages = snapshot.val()
      if (currentMessages != null){
        this.setState({
          messages: currentMessages
        })
      }
    })
  }

  render(){
    const allMessages = this.state.messages;
    const messageNodes = Object.keys(allMessages).map((messageId) => {
      return(
        <Message key={messageId} message={allMessages[messageId]}/>
      )
    })

    return (
      <div>
        <ul className="message-list">
          {messageNodes}
        </ul>
      </div>
    )
  }
}

export default MessageList
