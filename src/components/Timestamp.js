import React, { Component } from 'react';

class Timestamp extends Component {

  constructor(props){
    super(props)
    this.state = {
      nowStamp: new Date()
    }
  }

  componentDidMount(){
    //update the current time every 10 seconds
    this.timerID = setInterval(
      () => this.tick(),
      10000
    );
  }
  componentWillUnmount() {
    //clear timer when not needed
    clearInterval(this.timerID);
  }
  //callback for timestamp update
  tick() {
    this.setState({
      nowStamp: new Date()
    });
  }

  //determine time-difference between now and message timestamp
  timeDif(current, previous){
    var msPerMinute = 60 * 1000,
        msPerHour = msPerMinute * 60,
        msPerDay = msPerHour * 24,
        msPerMonth = msPerDay * 30,
        msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
      return 'just now';
    }
    else if (elapsed < msPerHour) {
      var res = Math.round(elapsed/msPerMinute);
      return res + ' minute' + ((res > 1 && res !== 0) ? 's' : '') + ' ago';
    }
    else if (elapsed < msPerDay ) {
      var res = Math.round(elapsed/msPerHour);
      return res + ' hour' + ((res > 1 && res !== 0) ? 's' : '') + ' ago';
    }
    else if (elapsed < msPerMonth) {
      var res = Math.round(elapsed/msPerDay);
      return 'about ' + res + ' day' + ((res > 1 && res !== 0) ? 's' : '') + ' ago';
    }
    else if (elapsed < msPerYear) {
      var res = Math.round(elapsed/msPerMonth);
      return 'about ' + res + ' month' + ((res > 1 && res !== 0) ? 's' : '') + ' ago';
    }
    else {
      var res = Math.round(elapsed/msPerYear);
      return 'about ' + res + ' year' + ((res > 1 && res !== 0) ? 's' : '') + ' ago';
    }
  }

  render(){
    const timestamp = this.props.timestamp;
    const nowStamp = this.state.nowStamp;

    return(
      <small className="timestamp">{this.timeDif(nowStamp, timestamp)}</small>
    )
  }
}

export default Timestamp
