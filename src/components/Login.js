import React, { Component } from 'react';

class Login extends Component {
  constructor(props){
    super(props)
    this.inputChange = this.inputChange.bind(this);
    this.userUpdate = this.userUpdate.bind(this);
    this.anonLogin = this.anonLogin.bind(this);
    this.providerLogin = this.providerLogin.bind(this);
    this.logout = this.logout.bind(this);
    this.state = {
      inputField: ''
    }
  }

  //update state on input change
  inputChange(e){
    const value = e.target.value.length ? e.target.value : '';
    this.setState({
      inputField: value
    })
  }

  //update user state in parent component
  userUpdate(user){
    this.props.onUserUpdate(user);
  }

  //handle log-out
  logout(){
    this.props.auth.signOut()
      .then(() => {
        this.props.onAuthLogout();
      })
  }

  //handle anonymous login
  anonLogin(){
    const inputData = this.state.inputField.trim();

    if (inputData.length){ //must have a nickname
      //firebase anon login
      this.props.auth.signInAnonymously()
        .then((user) => {
          //update username
          user.updateProfile({
            displayName: inputData
          }).then(() => {
            //bubble up state
            this.userUpdate(user);
          })
        }).catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
        });
    }
    //clear input field (for after log-out)
    this.setState({
      inputField: ''
    })
  }

  //handle provider login
  providerLogin(){
    const inputField = this.state.inputField.trim();

    //firebase provider login
    this.props.auth.signInWithPopup(this.props.provider)
      .then((result) => {
        let user = result.user;
        if (inputField){ //user entered a new nickname
          //update username
          user.updateProfile({
            displayName: inputField
          }).then(() => {
            //bubble up state
            this.userUpdate(user);
          })
        } else { //no new nickname
          //bubble up state
          this.userUpdate(user);
        }
      })
    //clear input field (for after log-out)
    this.setState({
      inputField: ''
    })
  }

  render(){
    const user = this.props.user;
    const inputField = this.state.inputField;

    let loginView = null;
    
    if (user){ //logged in
      loginView =
        <button onClick={this.logout}>Log Out</button>
    } else { //logged out
      loginView =
        <div>
          <input
            value={inputField}
            onChange={this.inputChange}
            placeholder="Choose a Nickname" /> <br/>
          <button onClick={this.anonLogin} disabled={!inputField.trim().length}>Log In as a guest</button> or <button onClick={this.providerLogin}>Log In with Google</button>
        </div>
    }
    return(
      <div>
        {loginView}
      </div>
    )
  }
}

export default Login
