import React, { Component } from 'react';
import Timestamp from './Timestamp';

function Message(props) {
  const timestamp = props.message.timestamp;
  return (
    <li className="message">
      <span className="user-name">{props.message.name}: </span>
      {props.message.message}<br/>
      <Timestamp timestamp={timestamp}/>
    </li>
  )
}

export default Message
