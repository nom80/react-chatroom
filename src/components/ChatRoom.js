import React, { Component } from 'react';
import MessageList from './MessageList';
import MessageBox from './MessageBox';

class ChatRoom extends Component {

  render(){
    const userName = this.props.user.displayName;
    return (
      <section>
        <MessageList db={this.props.db} />
        <MessageBox db={this.props.db} userName={userName} />
      </section>
    )
  }
}

export default ChatRoom
