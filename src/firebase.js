import firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBvRmBVYgrM488baLWDadwhu-xzT8A_1bM",
  authDomain: "react-chat-room-eb4a0.firebaseapp.com",
  databaseURL: "https://react-chat-room-eb4a0.firebaseio.com",
  projectId: "react-chat-room-eb4a0",
  storageBucket: "",
  messagingSenderId: "1032848174166"
};
firebase.initializeApp(config);

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();

export default firebase;
