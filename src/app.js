import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Header from './components/Header';
import ChatRoom from './components/ChatRoom';
import styles from './css/style.css';
import firebase, { auth, provider } from './firebase.js';

class App extends Component {

  constructor(props){
    super(props)
    this.handleUserUpdate = this.handleUserUpdate.bind(this);
    this.handleAuthLogout = this.handleAuthLogout.bind(this);
    this.state = {
      user: null
    }
  }

  componentDidMount(){
    //catch authentication events
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
      }
    })
  }

  //update state of user
  handleUserUpdate(user){
    this.setState({
      user
    })
  }

  //clear user state on logout
  handleAuthLogout(){
    this.setState({
      user: null
    });
  }

  render() {
    let user = this.state.user;

    let currentView = null;

    if (user){ //logged in
      currentView = <ChatRoom db={firebase} user={user} />;
    } //else, don't show view

    return (
      <div>
        <Header
          user={user}
          auth={auth} provider={provider}
          onUserUpdate={this.handleUserUpdate}
          onAuthLogout={this.handleAuthLogout}
        />
        {currentView}
      </div>
    );
  }
}

ReactDom.render(<App />, document.getElementById('app'))
